package ru.iteco.vetoshnikov.taskmanager.api.service;

import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import java.util.List;

public interface IProjectService {
    void createProject(@NotNull final Project project);

    void merge(@NotNull final Project project);

    List<Project> merge(@NotNull final List<Project> list);

    void remove(@NotNull final String key);

    void removeAllByUser(@NotNull final String userId);

    void clear();

    String getIdProject(@NotNull final String userId, @NotNull final String name);

    Project findOne(@NotNull final String key);

    @NotNull List<Project> findAll();

    void load(@NotNull final Domain domain);
}
