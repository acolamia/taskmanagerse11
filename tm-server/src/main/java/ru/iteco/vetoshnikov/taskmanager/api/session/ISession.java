package ru.iteco.vetoshnikov.taskmanager.api.session;

public interface ISession {
    void setTimestamp(Long timestamp);

    void setUserId(String userId);

    void setSignature(String signature);

    Long getTimestamp();

    String getUserId();

    String getSignature();
}
