package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    void mergeTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskObject") @Nullable Task taskObject
    );

    List<Task> mergeListTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskListObject") @Nullable List<Task> taskListObject
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskId") @Nullable String taskId
    );

    @WebMethod
    void clearTask(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    Task findOneTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskId") @Nullable String taskId
    );

    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    void createTaskTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "taskObject") @Nullable Task taskObject
    );

    @WebMethod
    void removeAllByProjectTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void loadTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "domainObject") @Nullable Domain domainObject
    );

    @WebMethod
    String getIdTaskTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "taskName") @Nullable String taskName
    );
}
