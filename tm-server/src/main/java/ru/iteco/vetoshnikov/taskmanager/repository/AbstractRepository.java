package ru.iteco.vetoshnikov.taskmanager.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.repository.IRepository;
import ru.iteco.vetoshnikov.taskmanager.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {
    @NotNull
    protected Map<String, E> repositoryMap = new ConcurrentHashMap<>();

    @Override
    public void merge(@NotNull final E e) {
        repositoryMap.put(e.getId(), e);
    }

    @Override
    public void remove(@NotNull final String key) {
        repositoryMap.remove(key);
    }

    @Override
    public void clear() {
        repositoryMap.clear();
    }

    @Override
    public E findOne(@NotNull final String key) {
        return repositoryMap.get(key);
    }

    @Override
    public List<E> findAll() {
        return new ArrayList<>(repositoryMap.values());
    }
}

