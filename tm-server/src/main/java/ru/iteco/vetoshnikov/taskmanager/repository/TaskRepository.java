package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.repository.ITaskRepository;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void persist(@NotNull final Task task) {
        for (@Nullable final Task taskInMap : repositoryMap.values()) {
            @Nullable final boolean isEqualUserId = task.getUserId().equals(taskInMap.getUserId());
            @Nullable final boolean isEqualProjectId = task.getProjectId().equals(taskInMap.getProjectId());
            @Nullable final boolean isEqualTaskName = task.getName().equals(taskInMap.getName());
            if (isEqualUserId && isEqualProjectId && isEqualTaskName)return;
        }
        repositoryMap.put(task.getId(), task);
    }

    @Override
    public void removeAllByProject(@NotNull final String key) {
        for (@NotNull final Task getTask : repositoryMap.values()) {
            if (key.equals(getTask.getProjectId())) {
                repositoryMap.remove(getTask.getId());
            }
        }
    }

    @Override
    public void load(@NotNull final Domain domain) {
        @NotNull final List<Task> domainTaskList = domain.getTaskList();
        for (@NotNull final Task getTask : domainTaskList) {
            repositoryMap.put(getTask.getId(), getTask);
        }
    }

    @Override
    public String getIdTask(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) {
        for (@NotNull final Task getTask : repositoryMap.values()) {
            @NotNull final boolean isEqualUserId = userId.equals(getTask.getUserId());
            @NotNull final boolean isEqualProjectId = projectId.equals(getTask.getProjectId());
            @NotNull final boolean isEqualTaskName = name.equals(getTask.getName());
            if (isEqualUserId && isEqualProjectId && isEqualTaskName) {
                return getTask.getId();
            }
        }
        return null;
    }
}
