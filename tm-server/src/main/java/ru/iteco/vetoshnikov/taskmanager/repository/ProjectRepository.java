package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.repository.IProjectRepository;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void persist(@NotNull final Project project)  {
        for (@Nullable final Project projectInMap : repositoryMap.values()) {
            @Nullable final boolean isEqualUserId = project.getUserId().equals(projectInMap.getUserId());
            @Nullable final boolean isEqualProjectName = project.getName().equals(projectInMap.getName());
            if (isEqualUserId && isEqualProjectName) return;
        }
        repositoryMap.put(project.getId(), project);
    }

    @Override
    public void removeAllByUser(@NotNull final String key) {
        for (@NotNull final Project getProject : repositoryMap.values()) {
            if (key.equals(getProject.getUserId())) {
                repositoryMap.remove(getProject.getId());
            }
        }
    }

    @Override
    public void load(@NotNull final Domain domain) {
        @NotNull final List<Project> domainProjectList = domain.getProjectList();
        for (@NotNull final Project getProject : domainProjectList) {
            repositoryMap.put(getProject.getId(), getProject);
        }
    }

    @Override
    public String getIdProject(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Project getProject : repositoryMap.values()) {
            @NotNull final boolean isEqualUserId = userId.equals(getProject.getUserId());
            @NotNull final boolean isEqualProjectName = name.equals(getProject.getName());
            if (isEqualUserId && isEqualProjectName) {
                return getProject.getId();
            }
        }
        return null;
    }
}
