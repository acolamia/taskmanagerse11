package ru.iteco.vetoshnikov.taskmanager.api.repository;

import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;

public interface IProjectRepository {
    void persist(@NotNull final Project project) ;

    void removeAllByUser(@NotNull final String key);

    void load(@NotNull final Domain domain);

    String getIdProject(@NotNull final String userId, @NotNull final String name);
}
