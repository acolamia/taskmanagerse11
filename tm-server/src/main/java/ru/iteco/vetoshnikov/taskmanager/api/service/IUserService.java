package ru.iteco.vetoshnikov.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IUserService {
    void createUser(@NotNull final User user) throws Exception;

    void merge(@NotNull final User user) throws Exception;

    List<User> merge(@NotNull final List<User> list) throws Exception;

    void remove(@NotNull final String key) throws Exception;

    String getIdUser(@NotNull final String userName) throws Exception;

    User findOne(@NotNull final String key) throws Exception;

    @NotNull List<User> findAll();

    void load(@NotNull final Domain domain) throws Exception;

    void clear() throws Exception;

    User checkPassword(@Nullable String userLogin, @Nullable String userPass);
}
