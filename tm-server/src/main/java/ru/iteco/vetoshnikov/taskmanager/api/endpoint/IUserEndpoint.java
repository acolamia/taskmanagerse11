package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {
    @WebMethod
    void createUserUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userObject") @Nullable User userObject
    );

    @WebMethod
    void mergeUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userObject") @Nullable User userObject
    );

    @WebMethod
    List<User> mergeListUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userList") @Nullable List<User> userList
    );

    @WebMethod
    void removeUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void clearUser(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    User findOneUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    List<User> findAllUser(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    void loadUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "domainObject") @Nullable Domain domainObject
    );

    @WebMethod
    String getIdUserUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userName") @Nullable String userName
    );

    @WebMethod
    Session getSession(
            @WebParam(name = "userLogin") @Nullable String userLogin,
            @WebParam(name = "userPass") @Nullable String userPass
    );
}
