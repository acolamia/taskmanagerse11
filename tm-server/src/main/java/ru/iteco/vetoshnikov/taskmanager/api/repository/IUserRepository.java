package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

public interface IUserRepository {
    void persist(@NotNull final User user);

    void load(@NotNull final Domain domain);

    String getIdUser(@NotNull final String name);
}
