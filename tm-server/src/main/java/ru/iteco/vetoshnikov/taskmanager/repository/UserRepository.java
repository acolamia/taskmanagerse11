package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.repository.IUserRepository;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void persist(@NotNull final User user) {
        for (@Nullable final User userInMap : repositoryMap.values()) {
            if (user.getName().equals(userInMap.getName())) return;
        }
        repositoryMap.put(user.getId(), user);
    }

    @Override
    public void load(@NotNull final Domain domain) {
        @NotNull final List<User> domainUserList = domain.getUserList();
        for (@NotNull final User getUser : domainUserList) {
            getRepositoryMap().put(getUser.getId(), getUser);
        }
    }

    @Override
    public String getIdUser(@NotNull final String login) {
        for (@NotNull final User getUser : getRepositoryMap().values()) {
            if (login.equals(getUser.getLogin())) {
                return getUser.getId();
            }
        }
        return null;
    }
}
