package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createUserUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userObject") @Nullable final User userObject
    ) {
        SignatureUtil.check(session);
        serviceLocator.getUserService().createUser(userObject);
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userObject") @Nullable final User userObject
    ) {
        SignatureUtil.check(session);
        serviceLocator.getUserService().merge(userObject);
    }

    @Override
    @WebMethod
    public List<User> mergeListUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userList") @Nullable final List<User> userList
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getUserService().merge(userList);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @Nullable final String userId
    ) {
        SignatureUtil.check(session);
        serviceLocator.getUserService().remove(userId);
    }

    @Override
    @WebMethod
    public void clearUser(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        SignatureUtil.check(session);
        serviceLocator.getUserService().clear();
    }

    @Override
    @WebMethod
    public User findOneUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @Nullable final String userId
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getUserService().findOne(userId);
    }

    @Override
    @WebMethod
    public List<User> findAllUser(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "domainObject") @Nullable final Domain domainObject
    ) {
        SignatureUtil.check(session);
        serviceLocator.getUserService().load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdUserUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userName") @Nullable final String userName
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getUserService().getIdUser(userName);
    }

    @Override
    public Session getSession(
        @WebParam(name = "userLogin") @Nullable final String userLogin,
        @WebParam(name = "userPass") @Nullable final String userPass) {
            @NonNull final Session session = new Session();
            @Nullable final User user = serviceLocator.getUserService().checkPassword(userLogin, userPass);
            if (user == null) return null;
            @NotNull final String userId = user.getId();
            session.setUserId(userId);
            @NotNull final Long timeStamp = new Date().getTime();
            session.setTimestamp(timeStamp);
            session.setSignature(SignatureUtil.sign(session));
            return session;
    }
}
