package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createProjectProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectObject") @Nullable final Project projectObject
    )  {
        SignatureUtil.check(session);
        serviceLocator.getProjectService().createProject(projectObject);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectObject") @Nullable final Project projectObject
    )  {
        SignatureUtil.check(session);
        serviceLocator.getProjectService().merge(projectObject);
    }

    @Override
    @WebMethod
    public List<Project> mergeListProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectListObject") @Nullable final List<Project> projectListObject
    )  {
        SignatureUtil.check(session);
        return serviceLocator.getProjectService().merge(projectListObject);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    )  {
        SignatureUtil.check(session);
        serviceLocator.getProjectService().remove(projectId);
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session") @Nullable final Session session
    )  {
        SignatureUtil.check(session);
        serviceLocator.getProjectService().clear();
    }

    @Override
    @WebMethod
    public Project findOneProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectName") @Nullable final String projectName
    )  {
        SignatureUtil.check(session);
        return serviceLocator.getProjectService().findOne(projectName);
    }

    @Override
    @WebMethod
    public List<Project> findAllProject(
            @WebParam(name = "session") @Nullable final Session session
    )  {
        SignatureUtil.check(session);
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    @WebMethod
    public void removeAllByUserProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @Nullable final String userId
    )  {
        SignatureUtil.check(session);
        serviceLocator.getProjectService().removeAllByUser(userId);
    }

    @Override
    @WebMethod
    public void loadProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "domainObject") @Nullable final Domain domainObject
    )  {
        SignatureUtil.check(session);
        serviceLocator.getProjectService().load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdProjectProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectName") @Nullable final String projectName
    )  {
        SignatureUtil.check(session);
        return serviceLocator.getProjectService().getIdProject(userId,projectName);
    }
}