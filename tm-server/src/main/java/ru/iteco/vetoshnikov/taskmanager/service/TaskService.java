package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {
    private final TaskRepository taskRepository;

    @Override
    public void createTask(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.persist(task);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.merge(task);
    }

    @Override
    public List<Task> merge(@Nullable final List<Task> taskList) {
        if (taskList == null || taskList.isEmpty()) return null;
        for (@Nullable final Task task : taskList) {
            if (task == null) continue;
            taskRepository.merge(task);
        }
        return taskList;
    }

    @Override
    public void remove(@Nullable final String key)  {
        if (key == null || key.isEmpty()) return;
        taskRepository.remove(key);
    }

    @Override
    public void removeAllByProject(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeAllByProject(projectId);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOne(@Nullable final String key) {
        if (key == null || key.isEmpty()) return null;
        return taskRepository.findOne(key);
    }

    @Override
    public @Nullable
    final List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        taskRepository.clear();
        taskRepository.load(domain);
    }

    @Override
    public String getIdTask(@Nullable final String userId, @Nullable final String projectId, @Nullable final String name)  {
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) return null;
        return taskRepository.getIdTask(userId, projectId, name);
    }
}