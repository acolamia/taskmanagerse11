package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createTaskTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskObject") @Nullable final Task taskObject
    ) {
        SignatureUtil.check(session);
        serviceLocator.getTaskService().createTask(taskObject);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskObject") @Nullable final Task taskObject
    ) {
        SignatureUtil.check(session);
        serviceLocator.getTaskService().merge(taskObject);
    }

    @Override
    @WebMethod
    public List<Task> mergeListTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskList") @Nullable final List<Task> taskList
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getTaskService().merge(taskList);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) {
        SignatureUtil.check(session);
        serviceLocator.getTaskService().remove(taskId);
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        SignatureUtil.check(session);
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    public Task findOneTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getTaskService().findOne(taskId);
    }

    @Override
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getTaskService().findAll();
    }

    @Override
    @WebMethod
    public void removeAllByProjectTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) {
        SignatureUtil.check(session);
        serviceLocator.getTaskService().removeAllByProject(projectId);
    }

    @Override
    @WebMethod
    public void loadTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "domainObject") @Nullable final Domain domainObject
    ) {
        SignatureUtil.check(session);
        serviceLocator.getTaskService().load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdTaskTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName") @Nullable final String taskName
    ) {
        SignatureUtil.check(session);
        return serviceLocator.getTaskService().getIdTask(userId, projectId, taskName);
    }
}
