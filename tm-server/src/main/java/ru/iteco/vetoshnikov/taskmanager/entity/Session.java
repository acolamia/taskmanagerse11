package ru.iteco.vetoshnikov.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.session.ISession;

@Getter
@Setter
@NoArgsConstructor
public final class Session implements ISession {
    @Nullable
    private Long timestamp;
    @Nullable
    private String userId;
    @Nullable
    private String signature;

}
