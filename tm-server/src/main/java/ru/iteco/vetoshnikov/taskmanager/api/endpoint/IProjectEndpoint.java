package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    void mergeProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectObject") @Nullable Project projectObject
    );

    List<Project> mergeListProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectListObject") @Nullable List<Project> projectListObject
    );

    @WebMethod
    void removeProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void clearProject(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    Project findOneProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    List<Project> findAllProject(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    void createProjectProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectObject") @Nullable Project projectObject
    );

    @WebMethod
    void removeAllByUserProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void loadProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "domainObject") @Nullable Domain domainObject
    );

    @WebMethod
    String getIdProjectProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectName") @Nullable String projectName
    );
}