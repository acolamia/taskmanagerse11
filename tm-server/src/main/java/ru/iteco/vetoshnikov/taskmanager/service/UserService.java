package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;
import ru.iteco.vetoshnikov.taskmanager.repository.UserRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class UserService extends AbstractService implements IUserService {
    private final UserRepository userRepository;

    @Override
    public void createUser(@Nullable final User user) {
        if (user == null) return;
        userRepository.persist(user);
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        userRepository.merge(user);
    }

    @Override
    public List<User> merge(@Nullable final List<User> userList) {
        if (userList == null || userList.isEmpty()) return null;
        for (@Nullable final User user : userList) {
            if (user == null) continue;
            userRepository.merge(user);
        }
        return userList;
    }

    @Override
    public void remove(@Nullable final String key) {
        if (key == null || key.isEmpty()) return;
        userRepository.remove(key);
    }

    @Override
    public User findOne(@Nullable final String key) {
        if (key == null || key.isEmpty()) return null;
        return userRepository.findOne(key);
    }

    @Override
    public @Nullable
    final List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        userRepository.clear();
        userRepository.load(domain);
    }

    @Override
    public String getIdUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.getIdUser(login);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public User checkPassword(
            @Nullable final String userLogin,
            @Nullable final String userPassword
    ) {
        if (userLogin == null || userLogin.isEmpty()) return null;
        if (userPassword == null || userPassword.isEmpty()) return null;
        @Nullable final List<User> userList = findAll();
        for (User getUser : userList) {
            if (userLogin.equals(getUser.getLogin())) {
                @NotNull final User user = getUser;
                @Nullable final String passwordHash = HashUtil.getHash(userPassword);
                if (passwordHash == null) return null;
                if (passwordHash.equals(user.getPassword())) return user;
            }
        }
        return null;
    }
}
