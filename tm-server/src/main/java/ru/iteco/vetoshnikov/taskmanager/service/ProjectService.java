package ru.iteco.vetoshnikov.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import lombok.*;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {
    private final ProjectRepository projectRepository;

    @Override
    public void createProject(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.persist(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.merge(project);
    }

    @Override
    public List<Project> merge(@Nullable final List<Project> projectList) {
        if (projectList == null || projectList.isEmpty()) return null;
        for (@Nullable final Project project : projectList) {
            if (project == null) continue;
            projectRepository.merge(project);
        }
        return projectList;
    }

    @Override
    public void remove(@Nullable final String key) {
        if (key == null || key.isEmpty()) return;
        projectRepository.remove(key);
    }

    @Override
    public void removeAllByUser(@Nullable final String userId)  {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUser(userId);
    }

    @Override
    public void clear(){
        projectRepository.clear();
    }

    @Override
    public Project findOne(@Nullable final String key) {
        if (key == null || key.isEmpty()) return null;
        return projectRepository.findOne(key);
    }

    @Override
    public @Nullable
    final List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        projectRepository.clear();
        projectRepository.load(domain);
    }

    @Override
    public String getIdProject(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty() || name == null || name.isEmpty()) return  null;
        return projectRepository.getIdProject(userId, name);
    }
}
