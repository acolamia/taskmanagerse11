package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "удаляет все задачи в веденном вами проекте.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getTaskEndpointService().getTaskEndpointPort().clearTask(session);
    }
}
