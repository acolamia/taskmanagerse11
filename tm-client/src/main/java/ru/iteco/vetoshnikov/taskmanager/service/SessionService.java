package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;

@Getter
@Setter
@NoArgsConstructor
public class SessionService {
    @Nullable
    private Session session = null;
}
