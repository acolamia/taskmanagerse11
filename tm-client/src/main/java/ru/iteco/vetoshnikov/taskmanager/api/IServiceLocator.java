package ru.iteco.vetoshnikov.taskmanager.api;


import ru.iteco.vetoshnikov.taskmanager.endpoint.DomainEndpointService;
import ru.iteco.vetoshnikov.taskmanager.endpoint.ProjectEndpointService;
import ru.iteco.vetoshnikov.taskmanager.endpoint.TaskEndpointService;
import ru.iteco.vetoshnikov.taskmanager.endpoint.UserEndpointService;
import ru.iteco.vetoshnikov.taskmanager.service.CommandService;
import ru.iteco.vetoshnikov.taskmanager.service.SessionService;

public interface IServiceLocator {

    SessionService getSessionService();

    ProjectEndpointService getProjectEndpointService();

    TaskEndpointService getTaskEndpointService();

    UserEndpointService getUserEndpointService();

    DomainEndpointService getDomainEndpointService();

    CommandService getCommandService();
}
