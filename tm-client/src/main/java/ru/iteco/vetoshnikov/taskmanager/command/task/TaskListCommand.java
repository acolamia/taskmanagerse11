package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "отображает список задач в веденном вами проекте.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        System.out.print("Введите имя проекта: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session, userId, projectName);
        @NotNull final List<Task> taskListAll = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findAllTask(session);
        System.out.println("Список задач:");
        for (@Nullable Task task : taskListAll) {
            @Nullable final boolean isEqualUserId = userId.equals(task.getUserId());
            @Nullable final boolean isEqualProjectId = projectId.equals(task.getProjectId());
            if (isEqualUserId && isEqualProjectId) {
                System.out.println(task.getName());
            }
        }
    }
}
