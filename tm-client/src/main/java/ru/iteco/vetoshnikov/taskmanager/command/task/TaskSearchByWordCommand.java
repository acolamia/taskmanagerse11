package ru.iteco.vetoshnikov.taskmanager.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class TaskSearchByWordCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-search";
    }

    @Override
    public String description() {
        return "поиск по названию задачи.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        if (userId == null) return;
        System.out.println("Введите название задачи:");
        @Nullable final String word = service.getScanner().nextLine();
        @Nullable final List<Task> taskListAll = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findAllTask(session);
        @Nullable final List<Task> taskList = new ArrayList<>();
        for (@Nullable final Task task : taskListAll) {
            if (task.getUserId().equals(userId)) {
                taskList.add(task);
            }
        }
        for (@Nullable final Task task : taskList) {
            if (task == null) continue;
            if (word.equalsIgnoreCase(task.getName())) System.out.println(task.getName());
        }
    }
}
