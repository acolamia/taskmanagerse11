package ru.iteco.vetoshnikov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.Nullable;

public final class UserLoginCommand extends AbstractCommand {
    public UserLoginCommand() {
        super();
        setSecure(false);
    }

    @Override
    public String command() {
        return "login";
    }

    @Override
    public String description() {
        return "Вызвать авторизацию пользователя.";
    }

    @Override
    public void execute() {
        System.out.print("Login: ");
        @Nullable final String tempLogin = service.getScanner().nextLine();
        System.out.print("Password: ");
        @Nullable final String tempPassword = service.getScanner().nextLine();
//        @Nullable final String hashTempPassword = HashUtil.getHash(tempPassword);
//        @Nullable final String loginId = serviceLocator.getUserEndpointService().getUserEndpointPort().getIdUserUser(session, tempLogin);
//        @Nullable final User user = serviceLocator.getUserEndpointService().getUserEndpointPort().findOneUser(session, loginId);


//        @NotNull final IUserEndpoint userEndpoint=serviceLocator.getUserEndpointService().getUserEndpointPort();
//        @NotNull final Session session = serviceLocator.getSessionService().getSession();
//        if (!hashTempPassword.equals(user.getPassword())) return;
//        if (session == null) {
//            System.out.println("Повторите попытку авторизации.");
//            return;
//        }
//        System.out.println("Вы зашли под пользователя: " + tempLogin);

        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();

        @Nullable final Session session = userEndpoint.getSession(tempLogin, tempPassword);

        if (session == null) {
            System.out.println("Повторите попытку авторизации.");
            return;
        }

        serviceLocator.getSessionService().setSession(session);
        System.out.println("Аторизация прошла успешно");
    }
}
