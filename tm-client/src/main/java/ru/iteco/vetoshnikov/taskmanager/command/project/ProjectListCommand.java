package ru.iteco.vetoshnikov.taskmanager.command.project;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "отображает список проектов.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        System.out.println("Список проектов:");
        @NotNull final List<Project> projectListAll = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findAllProject(session);
        for (@Nullable final Project project : projectListAll) {
            if (userId.equals(project.getUserId())) {
                System.out.println(project.getName());
            }
        }
    }
}
