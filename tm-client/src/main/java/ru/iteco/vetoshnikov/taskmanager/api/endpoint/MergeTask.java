
package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mergeTask complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mergeTask"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="taskObject" type="{http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/}task" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mergeTask", propOrder = {
    "session",
    "taskObject"
})
public class MergeTask {

    protected Session session;
    protected Task taskObject;

    /**
     * Gets the value of the session property.
     * 
     * @return
     *     possible object is
     *     {@link Session }
     *     
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     * 
     * @param value
     *     allowed object is
     *     {@link Session }
     *     
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the taskObject property.
     * 
     * @return
     *     possible object is
     *     {@link Task }
     *     
     */
    public Task getTaskObject() {
        return taskObject;
    }

    /**
     * Sets the value of the taskObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Task }
     *     
     */
    public void setTaskObject(Task value) {
        this.taskObject = value;
    }

}
