package ru.iteco.vetoshnikov.taskmanager.command.user;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.RoleType;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserSetPolicyCommand extends AbstractCommand {
    @Override
    public String command() {
        return "set-role";
    }

    @Override
    public String description() {
        return "Изменение прав доступа пользователя.";
    }

    @Override
    public void execute()  {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        System.out.print("Введите логин пользователя для которого хотите изменить права: ");
        @Nullable final String login = service.getScanner().nextLine();
        @Nullable final String loginId = serviceLocator.getUserEndpointService().getUserEndpointPort().getIdUserUser(session, login);
        @Nullable final User user = serviceLocator.getUserEndpointService().getUserEndpointPort().findOneUser(session, loginId);
        System.out.print("Введите ДА, если пользователю нужно дать права администратора: ");
        @NotNull final String tempUserRole = service.getScanner().nextLine();
        if (tempUserRole.equalsIgnoreCase("ДА")) {
            user.setRole(RoleType.ADMINISTRATOR);
            serviceLocator.getUserEndpointService().getUserEndpointPort().mergeUser(session, user);
            return;
        }
        user.setRole(RoleType.USER);
        serviceLocator.getUserEndpointService().getUserEndpointPort().mergeUser(session, user);
        return;
    }
}
