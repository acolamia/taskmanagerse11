package ru.iteco.vetoshnikov.taskmanager.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public final class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-removeall";
    }

    @Override
    public String description() {
        return "удаляет все ваши проекты.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        System.out.print("Введите имя проекта, который хотите удалить: ");
        @Nullable final String userId = session.getUserId();
        serviceLocator.getProjectEndpointService().getProjectEndpointPort().removeAllByUserProject(session, userId);
    }
}
