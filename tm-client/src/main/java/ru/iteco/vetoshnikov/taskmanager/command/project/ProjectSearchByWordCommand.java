package ru.iteco.vetoshnikov.taskmanager.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ProjectSearchByWordCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-search";
    }

    @Override
    public String description() {
        return "поиск по названию проекта.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @Nullable final String userId = session.getUserId();
        if (userId == null) return;
        System.out.println("Введите название проекта:");
        @Nullable final String word = service.getScanner().nextLine();
        @Nullable final List<Project> projectListAll = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findAllProject(session);
        @Nullable final List<Project> projectList = new ArrayList<>();
        for (@Nullable final Project project : projectListAll) {
            if (project.getUserId().equals(userId)) {
                projectList.add(project);
            }
        }
        for (@Nullable final Project project : projectList) {
            if (project == null) continue;
            if (word.equalsIgnoreCase(project.getName())) System.out.println(project.getName());
        }
    }
}
