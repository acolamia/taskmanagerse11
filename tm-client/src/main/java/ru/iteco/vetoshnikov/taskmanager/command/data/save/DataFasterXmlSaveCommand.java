package ru.iteco.vetoshnikov.taskmanager.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public class DataFasterXmlSaveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "data-save-fxml";
    }

    @Override
    public String description() {
        return "сохраняет в базу fasterXml.xml.";
    }

    @Override
    public void execute() {
        @NotNull final Session session=serviceLocator.getSessionService().getSession();
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().saveFasterXml(session);
        System.out.println("Выгрузка базы завершена");
    }
}
