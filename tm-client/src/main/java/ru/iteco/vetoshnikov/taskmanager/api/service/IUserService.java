package ru.iteco.vetoshnikov.taskmanager.api.service;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Domain;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;

import java.util.List;

public interface IUserService {
    void createUser(@NotNull final User user);

    void merge(@NotNull final User user);

    List<User> merge(@NotNull final List<User> list);

    void remove(@NotNull final String key);

    String getIdUser(@NotNull final String userName);

    User findOne(@NotNull final String key);

    @NotNull List<User> findAll();

    void load(@NotNull final Domain domain);

    void clear();
}
