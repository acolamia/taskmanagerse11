package ru.iteco.vetoshnikov.taskmanager.command.user;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;

@NoArgsConstructor
public final class UserSetPasswordCommand extends AbstractCommand {
    @Override
    public String command() {
        return "set-password";
    }

    @Override
    public String description() {
        return "Изменение пароля.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        System.out.print("Введите логин пользователя для которого хотите изменить пароль: ");
        @Nullable final String login = service.getScanner().nextLine();
        @Nullable final String loginId = serviceLocator.getUserEndpointService().getUserEndpointPort().getIdUserUser(session, login);
        System.out.print("Введите новый пароль: ");
        @Nullable final String newPassword = service.getScanner().nextLine();
        @Nullable final User user = serviceLocator.getUserEndpointService().getUserEndpointPort().findOneUser(session, loginId);
        user.setPassword(HashUtil.getHash(newPassword));
        serviceLocator.getUserEndpointService().getUserEndpointPort().mergeUser(session, user);

    }
}