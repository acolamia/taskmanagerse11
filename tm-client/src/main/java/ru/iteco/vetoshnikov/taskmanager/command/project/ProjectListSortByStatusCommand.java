package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.StatusType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class ProjectListSortByStatusCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list-sortbystatus";
    }

    @Override
    public String description() {
        return "отображает список проектов, сортируя по текущему статусу.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        System.out.println("Список проектов сотрированный по текущему статусу:");
        @NotNull final List<Project> projectList = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findAllProject(session);
        @NotNull final List<Project> thisUserProjectList = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            if (userId.equals(project.getUserId())) {
                thisUserProjectList.add(project);
            }
        }
        @NotNull final List<Project> statusListPlanned = new ArrayList<>();
        @NotNull final List<Project> statusListInprogress = new ArrayList<>();
        @NotNull final List<Project> statusListComplete = new ArrayList<>();
        for (@NotNull final Project getProject : thisUserProjectList) {
            @NotNull final boolean isStatusPlanned = getProject.getStatusType().equals(StatusType.PLANNED);
            @NotNull final boolean isStatusInprogress = getProject.getStatusType().equals(StatusType.INPROGRESS);
            @NotNull final boolean isStatusComplete = getProject.getStatusType().equals(StatusType.COMPLETE);
            if (isStatusPlanned) {
                statusListPlanned.add(getProject);
            }
            if (isStatusInprogress) {
                statusListInprogress.add(getProject);
            }
            if (isStatusComplete) {
                statusListComplete.add(getProject);
            }
        }
        @NotNull final List<Project> allList = new LinkedList<>();
        allList.addAll(statusListPlanned);
        allList.addAll(statusListInprogress);
        allList.addAll(statusListComplete);
        for (@NotNull final Project getProject : allList) {
            System.out.println(getProject.getName() + " - " + getProject.getStatusType());
        }
    }
}
