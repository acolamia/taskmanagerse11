package ru.iteco.vetoshnikov.taskmanager.command.task;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.*;

@NoArgsConstructor
public final class TaskListSortByStatusCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list-sortbystatus";
    }

    @Override
    public String description() {
        return "отображает список задач, сортируя по текущему статусу.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        System.out.println("Введите имя проекта, в котором нужнор произвести сортировку: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session, userId, projectName);
        System.out.println("Список проектов сотрированный по текущему статусу:");
        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findAllTask(session);
        @NotNull final List<Task> thisUserTaskList = new ArrayList<>();
        for (@Nullable final Task task : taskList) {
            @Nullable final boolean isEqualUserId = userId.equals(task.getUserId());
            @Nullable final boolean isEqualProjectId = projectId.equals(task.getProjectId());
            if (isEqualUserId && isEqualProjectId) {
                thisUserTaskList.add(task);
            }
        }
        @NotNull final List<Task> statusListPlanned = new ArrayList<>();
        @NotNull final List<Task> statusListInprogress = new ArrayList<>();
        @NotNull final List<Task> statusListComplete = new ArrayList<>();
        for (@NotNull final Task getTask : thisUserTaskList) {
            @NotNull final boolean isStatusPlanned = getTask.getStatusType().equals(StatusType.PLANNED);
            @NotNull final boolean isStatusInprogress = getTask.getStatusType().equals(StatusType.INPROGRESS);
            @NotNull final boolean isStatusComplete = getTask.getStatusType().equals(StatusType.COMPLETE);
            if (isStatusPlanned) {
                statusListPlanned.add(getTask);
            }
            if (isStatusInprogress) {
                statusListInprogress.add(getTask);
            }
            if (isStatusComplete) {
                statusListComplete.add(getTask);
            }
        }
        @NotNull final List<Task> allList = new LinkedList<>();
        allList.addAll(statusListPlanned);
        allList.addAll(statusListInprogress);
        allList.addAll(statusListComplete);
        for (@NotNull final Task getTask : allList) {
            System.out.println(getTask.getName() + " - " + getTask.getStatusType());
        }
    }
}