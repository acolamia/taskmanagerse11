package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.StatusType;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class ProjectSetStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "project-change-status";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет статус проекта.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @Nullable final String userId = session.getUserId();
        System.out.println("Введите имя проекта для которого нужно изменить статус: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session, userId, projectName);
        System.out.println("Введите статус проекта из списка:");
        for (@Nullable StatusType statusType : StatusType.values()) {
            System.out.println(statusType);
        }
        @Nullable final String newSetStatus = service.getScanner().nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findOneProject(session, projectId);
        if (newSetStatus.equalsIgnoreCase("Запланировано")) {
            project.setStatusType(ru.iteco.vetoshnikov.taskmanager.api.endpoint.StatusType.PLANNED);
            serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(session, project);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("В процессе")) {
            project.setStatusType(ru.iteco.vetoshnikov.taskmanager.api.endpoint.StatusType.INPROGRESS);
            serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(session, project);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("Выполнено")) {
            project.setStatusType(ru.iteco.vetoshnikov.taskmanager.api.endpoint.StatusType.COMPLETE);
            serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(session, project);
            return;
        }
    }
}
