package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.util.DateUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor
public final class TaskSetBeginDateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-change-begindate";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет время начала задачи.";
    }

    @Override
    public void execute() throws DatatypeConfigurationException {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        System.out.println("Введите имя задачи которой нужно изменить время начала: ");
        @Nullable final String taskName = service.getScanner().nextLine();
        System.out.println("В каком проекте находится эта задача: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        System.out.println("Введите дату начала проекта в формате ДД.ММ.ГГГГ: ");
        @Nullable final String newBeginDateString = service.getScanner().nextLine();
        @Nullable final Date newDate = DateUtil.parseDate(newBeginDateString);
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session, userId, projectName);
        @Nullable final String taskId = serviceLocator.getTaskEndpointService().getTaskEndpointPort().getIdTaskTask(session, userId, projectId, taskName);
        @Nullable final Task task = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findOneTask(session, taskId);

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(newDate);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        task.setBeginDate(xmlGregorianCalendar);

        task.setBeginDate(xmlGregorianCalendar);
        serviceLocator.getTaskEndpointService().getTaskEndpointPort().mergeTask(session, task);
    }
}
