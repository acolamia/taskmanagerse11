package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.StatusType;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class TaskSetStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "task-change-status";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет статус задачи.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @Nullable final String userId = session.getUserId();
        System.out.println("Введите имя задачи для которой нужно изменить статус: ");
        @Nullable final String taskName = service.getScanner().nextLine();
        System.out.println("В каком проекте находится эта задача: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session, userId, projectName);
        @Nullable final String taskId = serviceLocator.getTaskEndpointService().getTaskEndpointPort().getIdTaskTask(session, userId, projectId, taskName);
        System.out.println("Введите статус проекта из списка:");
        for (@Nullable StatusType statusType : StatusType.values()) {
            System.out.println(statusType);
        }
        @Nullable final String newSetStatus = service.getScanner().nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpointService().getTaskEndpointPort().findOneTask(session, taskId);
        if (newSetStatus.equalsIgnoreCase("Запланировано")) {
            task.setStatusType(StatusType.PLANNED);
            serviceLocator.getTaskEndpointService().getTaskEndpointPort().mergeTask(session, task);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("В процессе")) {
            task.setStatusType(StatusType.INPROGRESS);
            serviceLocator.getTaskEndpointService().getTaskEndpointPort().mergeTask(session, task);
            return;
        }
        if (newSetStatus.equalsIgnoreCase("Выполнено")) {
            task.setStatusType(StatusType.COMPLETE);
            serviceLocator.getTaskEndpointService().getTaskEndpointPort().mergeTask(session, task);
            return;
        }
    }
}
