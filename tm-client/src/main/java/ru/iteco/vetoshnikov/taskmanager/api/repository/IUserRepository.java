package ru.iteco.vetoshnikov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Domain;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.User;

public interface IUserRepository {
    void persist(@NotNull final User user);

    void load(@NotNull final Domain domain);

    String getIdUser(@NotNull final String name);
}
