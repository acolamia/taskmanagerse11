package ru.iteco.vetoshnikov.taskmanager.command.task;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Task;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "создается задача в веденном вами проекте.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        System.out.print("Введите имя новой задачи: ");
        @Nullable final String taskName = service.getScanner().nextLine();
        System.out.print("В какой проект положить данную задачу? : ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session, userId, projectName);
        @Nullable final Task task = new Task();
        task.setName(taskName);
        task.setUserId(userId);
        task.setProjectId(projectId);
        serviceLocator.getTaskEndpointService().getTaskEndpointPort().createTaskTask(session, task);
    }
}
