package ru.iteco.vetoshnikov.taskmanager.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;

@NoArgsConstructor
public final class TaskRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-removeall";
    }

    @Override
    public String description() {
        return "удаляет все ваши задачи в проекте.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @NotNull final String userId = session.getUserId();
        System.out.println("В каком проекте произвести очистку задач: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session, userId, projectName);
        serviceLocator.getTaskEndpointService().getTaskEndpointPort().removeAllByProjectTask(session, projectId);
    }
}
