package ru.iteco.vetoshnikov.taskmanager.command.project;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Project;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import ru.iteco.vetoshnikov.taskmanager.util.DateUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@NoArgsConstructor
public final class ProjectSetBeginDateCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "project-change-begindate";
    }

    @Override
    public @NotNull String description() {
        return "Изменяет время начала проекта.";
    }

    @Override
    public void execute() throws DatatypeConfigurationException {
        @NotNull final Session session = serviceLocator.getSessionService().getSession();
        @Nullable final String userId = session.getUserId();
        System.out.println("Введите имя проекта для которого нужно изменить время начала: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        System.out.println("Введите дату начала проекта в формате ДД.ММ.ГГГГ: ");
        @Nullable final String newBeginDateString = service.getScanner().nextLine();
        @Nullable final Date newDate = DateUtil.parseDate(newBeginDateString);
        @Nullable final String projectId = serviceLocator.getProjectEndpointService().getProjectEndpointPort().getIdProjectProject(session,userId, projectName);
        @Nullable final Project project = serviceLocator.getProjectEndpointService().getProjectEndpointPort().findOneProject(session,projectId);

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(newDate);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        project.setBeginDate(xmlGregorianCalendar);

        project.setBeginDate(xmlGregorianCalendar);
        serviceLocator.getProjectEndpointService().getProjectEndpointPort().mergeProject(session,project);
    }
}
