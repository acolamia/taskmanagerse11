
package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.iteco.vetoshnikov.taskmanager.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LoadBinary_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadBinary");
    private final static QName _LoadBinaryResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadBinaryResponse");
    private final static QName _LoadFasterJson_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadFasterJson");
    private final static QName _LoadFasterJsonResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadFasterJsonResponse");
    private final static QName _LoadFasterXml_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadFasterXml");
    private final static QName _LoadFasterXmlResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadFasterXmlResponse");
    private final static QName _LoadJaxbJson_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadJaxbJson");
    private final static QName _LoadJaxbJsonResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadJaxbJsonResponse");
    private final static QName _LoadJaxbXml_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadJaxbXml");
    private final static QName _LoadJaxbXmlResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "loadJaxbXmlResponse");
    private final static QName _SaveBinary_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveBinary");
    private final static QName _SaveBinaryResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveBinaryResponse");
    private final static QName _SaveFasterJson_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveFasterJson");
    private final static QName _SaveFasterJsonResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveFasterJsonResponse");
    private final static QName _SaveFasterXml_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveFasterXml");
    private final static QName _SaveFasterXmlResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveFasterXmlResponse");
    private final static QName _SaveJaxbJson_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveJaxbJson");
    private final static QName _SaveJaxbJsonResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveJaxbJsonResponse");
    private final static QName _SaveJaxbXml_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveJaxbXml");
    private final static QName _SaveJaxbXmlResponse_QNAME = new QName("http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", "saveJaxbXmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.iteco.vetoshnikov.taskmanager.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoadBinary }
     * 
     */
    public LoadBinary createLoadBinary() {
        return new LoadBinary();
    }

    /**
     * Create an instance of {@link LoadBinaryResponse }
     * 
     */
    public LoadBinaryResponse createLoadBinaryResponse() {
        return new LoadBinaryResponse();
    }

    /**
     * Create an instance of {@link LoadFasterJson }
     * 
     */
    public LoadFasterJson createLoadFasterJson() {
        return new LoadFasterJson();
    }

    /**
     * Create an instance of {@link LoadFasterJsonResponse }
     * 
     */
    public LoadFasterJsonResponse createLoadFasterJsonResponse() {
        return new LoadFasterJsonResponse();
    }

    /**
     * Create an instance of {@link LoadFasterXml }
     * 
     */
    public LoadFasterXml createLoadFasterXml() {
        return new LoadFasterXml();
    }

    /**
     * Create an instance of {@link LoadFasterXmlResponse }
     * 
     */
    public LoadFasterXmlResponse createLoadFasterXmlResponse() {
        return new LoadFasterXmlResponse();
    }

    /**
     * Create an instance of {@link LoadJaxbJson }
     * 
     */
    public LoadJaxbJson createLoadJaxbJson() {
        return new LoadJaxbJson();
    }

    /**
     * Create an instance of {@link LoadJaxbJsonResponse }
     * 
     */
    public LoadJaxbJsonResponse createLoadJaxbJsonResponse() {
        return new LoadJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link LoadJaxbXml }
     * 
     */
    public LoadJaxbXml createLoadJaxbXml() {
        return new LoadJaxbXml();
    }

    /**
     * Create an instance of {@link LoadJaxbXmlResponse }
     * 
     */
    public LoadJaxbXmlResponse createLoadJaxbXmlResponse() {
        return new LoadJaxbXmlResponse();
    }

    /**
     * Create an instance of {@link SaveBinary }
     * 
     */
    public SaveBinary createSaveBinary() {
        return new SaveBinary();
    }

    /**
     * Create an instance of {@link SaveBinaryResponse }
     * 
     */
    public SaveBinaryResponse createSaveBinaryResponse() {
        return new SaveBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveFasterJson }
     * 
     */
    public SaveFasterJson createSaveFasterJson() {
        return new SaveFasterJson();
    }

    /**
     * Create an instance of {@link SaveFasterJsonResponse }
     * 
     */
    public SaveFasterJsonResponse createSaveFasterJsonResponse() {
        return new SaveFasterJsonResponse();
    }

    /**
     * Create an instance of {@link SaveFasterXml }
     * 
     */
    public SaveFasterXml createSaveFasterXml() {
        return new SaveFasterXml();
    }

    /**
     * Create an instance of {@link SaveFasterXmlResponse }
     * 
     */
    public SaveFasterXmlResponse createSaveFasterXmlResponse() {
        return new SaveFasterXmlResponse();
    }

    /**
     * Create an instance of {@link SaveJaxbJson }
     * 
     */
    public SaveJaxbJson createSaveJaxbJson() {
        return new SaveJaxbJson();
    }

    /**
     * Create an instance of {@link SaveJaxbJsonResponse }
     * 
     */
    public SaveJaxbJsonResponse createSaveJaxbJsonResponse() {
        return new SaveJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link SaveJaxbXml }
     * 
     */
    public SaveJaxbXml createSaveJaxbXml() {
        return new SaveJaxbXml();
    }

    /**
     * Create an instance of {@link SaveJaxbXmlResponse }
     * 
     */
    public SaveJaxbXmlResponse createSaveJaxbXmlResponse() {
        return new SaveJaxbXmlResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadBinary")
    public JAXBElement<LoadBinary> createLoadBinary(LoadBinary value) {
        return new JAXBElement<LoadBinary>(_LoadBinary_QNAME, LoadBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadBinaryResponse")
    public JAXBElement<LoadBinaryResponse> createLoadBinaryResponse(LoadBinaryResponse value) {
        return new JAXBElement<LoadBinaryResponse>(_LoadBinaryResponse_QNAME, LoadBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadFasterJson")
    public JAXBElement<LoadFasterJson> createLoadFasterJson(LoadFasterJson value) {
        return new JAXBElement<LoadFasterJson>(_LoadFasterJson_QNAME, LoadFasterJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadFasterJsonResponse")
    public JAXBElement<LoadFasterJsonResponse> createLoadFasterJsonResponse(LoadFasterJsonResponse value) {
        return new JAXBElement<LoadFasterJsonResponse>(_LoadFasterJsonResponse_QNAME, LoadFasterJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadFasterXml")
    public JAXBElement<LoadFasterXml> createLoadFasterXml(LoadFasterXml value) {
        return new JAXBElement<LoadFasterXml>(_LoadFasterXml_QNAME, LoadFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadFasterXmlResponse")
    public JAXBElement<LoadFasterXmlResponse> createLoadFasterXmlResponse(LoadFasterXmlResponse value) {
        return new JAXBElement<LoadFasterXmlResponse>(_LoadFasterXmlResponse_QNAME, LoadFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadJaxbJson")
    public JAXBElement<LoadJaxbJson> createLoadJaxbJson(LoadJaxbJson value) {
        return new JAXBElement<LoadJaxbJson>(_LoadJaxbJson_QNAME, LoadJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadJaxbJsonResponse")
    public JAXBElement<LoadJaxbJsonResponse> createLoadJaxbJsonResponse(LoadJaxbJsonResponse value) {
        return new JAXBElement<LoadJaxbJsonResponse>(_LoadJaxbJsonResponse_QNAME, LoadJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadJaxbXml")
    public JAXBElement<LoadJaxbXml> createLoadJaxbXml(LoadJaxbXml value) {
        return new JAXBElement<LoadJaxbXml>(_LoadJaxbXml_QNAME, LoadJaxbXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "loadJaxbXmlResponse")
    public JAXBElement<LoadJaxbXmlResponse> createLoadJaxbXmlResponse(LoadJaxbXmlResponse value) {
        return new JAXBElement<LoadJaxbXmlResponse>(_LoadJaxbXmlResponse_QNAME, LoadJaxbXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveBinary")
    public JAXBElement<SaveBinary> createSaveBinary(SaveBinary value) {
        return new JAXBElement<SaveBinary>(_SaveBinary_QNAME, SaveBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveBinaryResponse")
    public JAXBElement<SaveBinaryResponse> createSaveBinaryResponse(SaveBinaryResponse value) {
        return new JAXBElement<SaveBinaryResponse>(_SaveBinaryResponse_QNAME, SaveBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveFasterJson")
    public JAXBElement<SaveFasterJson> createSaveFasterJson(SaveFasterJson value) {
        return new JAXBElement<SaveFasterJson>(_SaveFasterJson_QNAME, SaveFasterJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveFasterJsonResponse")
    public JAXBElement<SaveFasterJsonResponse> createSaveFasterJsonResponse(SaveFasterJsonResponse value) {
        return new JAXBElement<SaveFasterJsonResponse>(_SaveFasterJsonResponse_QNAME, SaveFasterJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveFasterXml")
    public JAXBElement<SaveFasterXml> createSaveFasterXml(SaveFasterXml value) {
        return new JAXBElement<SaveFasterXml>(_SaveFasterXml_QNAME, SaveFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveFasterXmlResponse")
    public JAXBElement<SaveFasterXmlResponse> createSaveFasterXmlResponse(SaveFasterXmlResponse value) {
        return new JAXBElement<SaveFasterXmlResponse>(_SaveFasterXmlResponse_QNAME, SaveFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveJaxbJson")
    public JAXBElement<SaveJaxbJson> createSaveJaxbJson(SaveJaxbJson value) {
        return new JAXBElement<SaveJaxbJson>(_SaveJaxbJson_QNAME, SaveJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveJaxbJsonResponse")
    public JAXBElement<SaveJaxbJsonResponse> createSaveJaxbJsonResponse(SaveJaxbJsonResponse value) {
        return new JAXBElement<SaveJaxbJsonResponse>(_SaveJaxbJsonResponse_QNAME, SaveJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveJaxbXml")
    public JAXBElement<SaveJaxbXml> createSaveJaxbXml(SaveJaxbXml value) {
        return new JAXBElement<SaveJaxbXml>(_SaveJaxbXml_QNAME, SaveJaxbXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.vetoshnikov.iteco.ru/", name = "saveJaxbXmlResponse")
    public JAXBElement<SaveJaxbXmlResponse> createSaveJaxbXmlResponse(SaveJaxbXmlResponse value) {
        return new JAXBElement<SaveJaxbXmlResponse>(_SaveJaxbXmlResponse_QNAME, SaveJaxbXmlResponse.class, null, value);
    }

}
